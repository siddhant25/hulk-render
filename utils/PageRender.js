const ejs = require("ejs");
const Populator = require('./Populator.js');

class PageRender {
  constructor(template, dynamicSectionKeys, apiPath = "http://localhost/nuSource/api/v1/") {
    this.template = template;
    this.dynamicSectionKeys = dynamicSectionKeys
    // this.apiPath = apiPath;
    this.populator = new Populator(apiPath);
  }

  sectionRenderMap = async() => {
    let template_dependencies = {}
    template_dependencies.dependencies = {};
    let rendering = this.dynamicSectionKeys.map(async (keyObj) => {
      // console.log(keyObj.settings);
      // return;
      let populatedTemplateDependencies = await this.populator.populatePlaceHolders(keyObj.key, keyObj.settings);
      template_dependencies.dependencies[keyObj.setting_id] = populatedTemplateDependencies;
    });
    const resolved = await Promise.all(rendering);
    // console.log(template_dependencies);
    return template_dependencies;
  };

  renderPage = async(renderedPlaceHolders) => {
    try{
      let renderedPage = await ejs.render(this.template, renderedPlaceHolders);
      // console.log(renderedPage);
      return renderedPage;
    }
    catch(error){
      console.log(error);
      return;
    }
  }

}

module.exports = PageRender;
