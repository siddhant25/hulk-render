const endPointMapper = {
    populateAllCategories: (paramObj) => {
        return `builder/institute/${paramObj.institution_id}/course/categories`;
    },
    populateCourseInstructors: (paramObj) => {
        return `organization/bundles/${paramObj.institution_bundle_id}?get_tutors=1`;
    },
    populateCourseShortInfo: (paramObj) => {
        return `organization/bundles/${paramObj.institution_bundle_id}?get_tutors=1`;
    },
    populateCourseSchedule: (paramObj) => {
        return `bundles/${paramObj.bundle_id}/masterbatches?organization_id=${paramObj.org_id}`
    },
    populateCourseCurriculum: (paramObj) => {
        return `public/tutor/courses/all/${paramObj.institution_bundle_id}`
    },
    populateCourseOverview: (paramObj) => {
        return `organization/bundles/${paramObj.institution_bundle_id}?get_tutors=1`
    },
    populateCourseBanner: (paramObj) => {
        return `organization/bundles/${paramObj.institution_bundle_id}?get_tutors=1`
    }

}

module.exports = endPointMapper;