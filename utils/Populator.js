const axios = require("axios");
const ejs = require('ejs');
const endPointMapper = require('./PopulatorEndPointMapper');
const SubTemplateRender = require("./SubTemplateRender");

class Populator {
    constructor(apiPath) {
        this.apiPath = apiPath;
        this.identifierMapper = {
            all_categories: this.allCategories,
            course_banner: this.courseBanner,
            course_instructors: this.courseInstructors,
            course_short_info: this.courseShortInfo,
            course_schedule: this.courseSchedule,
            course_curriculum: this.courseCurriculum,
            course_overview: this.courseOverview,
            trial: this.trialFunc,
        };
        this.apiCalls = {}
        let hostOrigin = apiPath.replace("/nuSource/api/v1/", "");
        hostOrigin = hostOrigin.replace("-api.", ".");
        this.subTemplateRender = new SubTemplateRender(hostOrigin);
    }

    populatePlaceHolders = async (key, settings) => {
        if (key in this.identifierMapper) {
            return await this.identifierMapper[key](settings);
        }
    }

    fetcher = (url) => {
        // console.log(this.apiCalls);
        return new Promise(async (resolve, reject) => {
            try {
                if (url in this.apiCalls) {
                    resolve(await this.apiCalls[url]);
                }
                else {
                    // console.log('called');
                    this.apiCalls[url] = axios({ method: "GET", url: url })
                    resolve(await this.apiCalls[url]);
                }
            }
            catch (error) {
                reject(error);
            }
        });
    };

    batchRequests = (requests) => {
        return Promise.all(requests)
    };

    trialFunc = (settings) => {
        let template_dependencies = {
            setting_id: settings.setting_id,
            name: `name-${settings.setting_id}-<%= variable %>`
        }
        return template_dependencies;
    }

    populateCourseBanner = (settings) => {
        let url = this.apiPath + endPointMapper.populateCourseBanner(settings);
        return this.fetcher(url);
    }

    courseBanner = async (settings) => {
        let template_dependencies = { courseTitle: "", courseDescription: "", courseBannerRating: "", courseBannerInstructorList: "", courseBannerPricing: "", courseBannerPurchaseLink: "", courseBannerMedia: ""}
        try {
            const Data = await this.batchRequests([this.populateCourseBanner(settings)]);
            const bundleData = Data[0].data;
            template_dependencies.courseTitle = bundleData.bundle.bundle_name;
            template_dependencies.courseDescription = bundleData.bundle.bundle_description;
            template_dependencies.courseBannerRating = await this.subTemplateRender.renderCourseBannerRating(bundleData.bundle, 'courseBannerRating');
            template_dependencies.courseBannerInstructorList = await this.subTemplateRender.renderCourseBannerInstructorList(bundleData.tutors, 'courseBannerInstructorList');
            template_dependencies.courseBannerPricing = await this.subTemplateRender.renderCourseBannerPricing(bundleData.bundle, 'courseBannerPricing');
            template_dependencies.courseBannerPurchaseLink = await this.subTemplateRender.renderCourseBannerPurchaseLink(bundleData.bundle, 'courseBannerPurchaseLink');
            template_dependencies.courseBannerMedia = await this.subTemplateRender.renderCourseBannerMedia(bundleData.bundle, 'courseBannerMedia');
            return template_dependencies;
        } catch (error) {
            console.log(error);
        }
    }

    populateAllCategories = (settings) => {
        let url = this.apiPath + endPointMapper.populateAllCategories(settings);
        return this.fetcher(url);
    }

    allCategories = async (settings) => {
        try {
            let template_dependencies = {
                categoryListing: ""
            }
            const Data = await this.batchRequests([this.populateAllCategories(settings)])
            const response = Data[0];
            const categories = response.data
            // console.log('categories ---------------------------------------->', categories);
            template_dependencies.categoryListing = await this.subTemplateRender.renderCategoryListing(categories, 'categoryListing');
            return template_dependencies;
        }
        catch (error) {
            console.log(error);
        }
    }

    populateCourseInstructors = (settings) => {
        let url = this.apiPath + endPointMapper.populateCourseInstructors(settings);
        return this.fetcher(url);
    }

    courseInstructors = async (settings) => {
        try {
            let template_dependencies = {
                courseInstuctorListing: ""
            };
            const Data = await this.batchRequests([this.populateCourseInstructors(settings)]);
            const response = Data[0];
            const bundleData = response.data;
            // console.log('instructors ------------------------------------------->', bundleData);
            template_dependencies.courseInstuctorListing = await this.subTemplateRender.renderCourseInstructorListing(bundleData.tutors, 'courseInstuctorListing');
            return template_dependencies;
        }
        catch (error) {
            console.log(error);
        }
    }

    populateCourseShortInfo = (settings) => {
        let url = this.apiPath + endPointMapper.populateCourseShortInfo(settings);
        return this.fetcher(url);
    }

    courseShortInfo = async (settings) => {
        try {
            let template_dependencies = { packageType: "Online Program", orgName: "", packageInfoCard: "", installmentInfoCard: "", freePreviewInfoCard: ""}
            const Data = await this.batchRequests([this.populateCourseShortInfo(settings)]);
            const response = Data[0];
            const bundle = response.data.bundle;
            // console.log('bundleData----------------------------------------------->', bundle, this.apiCalls);
            template_dependencies.packageInfoCard = await this.subTemplateRender.renderPackageInfoCard(bundle, 'packageInfoCard');
            template_dependencies.installmentInfoCard = await this.subTemplateRender.renderInstallmentInfoCard(bundle.fees_template_ids.length - 1, 'installmentInfoCard');
            template_dependencies.freePreviewInfoCard = await this.subTemplateRender.renderFreePreviewInfoCard(bundle.free_preview_allowed, 'freePreviewInfoCard');
            return template_dependencies;
        }
        catch (error) {
            console.log(error);
        }
    }

    populateCourseSchedule = (settings) => {
        let url = this.apiPath + endPointMapper.populateCourseSchedule(settings);
        return this.fetcher(url);
    }

    courseSchedule = async (settings) => {
        try {
            let template_dependencies = { 
                startDate: 0, 
                endDate: 0, 
                numOfClasses: 0 
            };
            const Data = await this.batchRequests([this.populateCourseSchedule(settings)]);
            const response = Data[0];
            const scheduleData = response.data;
            template_dependencies.startDate = new Date(scheduleData.master_batches[0].start_date_unix * 1000).toDateString().split(' ').slice(1).join(" ");
            template_dependencies.endDate = new Date(scheduleData.master_batches[0].end_date_unix * 1000).toDateString().split(' ').slice(1).join(" ");
            template_dependencies.numOfClasses = scheduleData.master_batches[0].total_classes;
            return template_dependencies;
        }
        catch (error) {
            console.log(error);
        }
    }

    populateCourseCurriculum = (settings) => {
        let url = this.apiPath + endPointMapper.populateCourseCurriculum(settings);
        return this.fetcher(url);
    }

    getSubjectCurriculum = async (subjects, instBundleId) => {
        if(!subjects) return;
        let curriculumObj = {};
        let completeCurriculum = subjects.map(async (subject) => {
            let serviceUrl = this.apiPath + `public/tutor/class/curriculum/${subject.course_id}?institution_bundle_id=${instBundleId}`
            let sectionData = await this.fetcher(serviceUrl);
            sectionData = sectionData.data;
            // console.log(sectionData);
            curriculumObj[subject.course_id] = sectionData.course_curriculum ? sectionData.course_curriculum : [];
        });
        const resolved = await Promise.all(completeCurriculum);
        // console.log(curriculumObj, 'currii');
        return curriculumObj;
    }

    courseCurriculum = async (settings) => {
        let template_dependencies = { numOfSubjects: 0, curriculumListing: "" }
        const Data = await this.batchRequests([this.populateCourseCurriculum(settings)]);
        let courseData = Data[0].data;
        const subjects = courseData.courses ? courseData.courses: [];
        const curriculumObj = await this.getSubjectCurriculum(subjects, settings.institution_bundle_id);
        template_dependencies.numOfSubjects = subjects.length;
        template_dependencies.curriculumListing = await this.subTemplateRender.renderCourseCurriculum(subjects, curriculumObj, 'curriculumListing');
        return template_dependencies;
    }

    populateCourseOverview = (settings) => {
        let url = this.apiPath + endPointMapper.populateCourseOverview(settings)
        return this.fetcher(url);
    }

    courseOverview = async (settings) => {
        let template_dependencies = { courseOverview: "" };
        const Data = await this.batchRequests([this.populateCourseOverview(settings)]);
        const bundleData = Data[0].data;
        template_dependencies.courseOverview = bundleData.bundle.bundle_overview;
        // console.log('bundleData', bundleData);
        return template_dependencies;
    }

}

module.exports = Populator;