const ejs = require('ejs');
const PlaceholderSections = require('../sections/placeholder-sections');

class SubTemplateRender{
    constructor(hostOrigin){
        this.hostOrigin = hostOrigin
    }

    renderSubTemplate = async (identifier, subTemplateVariables) => {
        return await ejs.render(PlaceholderSections[identifier], subTemplateVariables);
    }

    renderCategoryListing = async(categoriesResponse, identifier) => {
        let subTemplateVariables = {
            hostName: this.hostOrigin,
            categories: [],
            course_counts: []
        }
        categoriesResponse.category.map((category) => {
            subTemplateVariables.categories.push({ category_name: category.stream_name, category_id: category.stream_id });
            let course_count = category.sub_categories.reduce((accumulator, subCategory) => accumulator + subCategory.course_count, 0);
            subTemplateVariables.course_counts.push(course_count);
        });
        return await this.renderSubTemplate(identifier, subTemplateVariables);
    }

    renderCourseInstructorListing = async (tutorData, identifier) => {
        let subTemplateVariables = { tutors: tutorData ? tutorData : [] }
        return await this.renderSubTemplate(identifier, subTemplateVariables);
    }

    renderCourseCurriculum = async (subjects, curriculumObj, identifier) => {
        let subTemplateVariables = {
            subjects: subjects,
            curriculumObj: curriculumObj
        }
        return await this.renderSubTemplate(identifier, subTemplateVariables);
    }

    renderInstallmentInfoCard = async (installments, identifier) => {
        let subTemplateVariables = { installments: installments};
        return await this.renderSubTemplate(identifier, subTemplateVariables);
    }

    renderFreePreviewInfoCard = async (freePreviewAllowed, identifier) => {
        let subTemplateVariables = { freePreviewAllowed: freePreviewAllowed};
        return await this.renderSubTemplate(identifier, subTemplateVariables);
    }

    renderCourseBannerPricing = async (bundle, identifier) => {
        let subTemplateVariables = {
            position: bundle.position,
            striked_cost: bundle.striked_cost,
            actual_cost: bundle.actual_cost,
            currency_symbol: bundle.currency_symbol
        }
        return await this.renderSubTemplate(identifier, subTemplateVariables);
    }

    renderCourseBannerInstructorList = async (tutors, identifier) => {
        let subTemplateVariables = {
            tutors: tutors ? tutors : [],
        }
        return await this.renderSubTemplate(identifier, subTemplateVariables);
    }

    renderCourseBannerRating = async (bundle, identifier) => {
        let subTemplateVariables = {
            Rating: bundle.rating,
            num_of_rating: bundle.num_of_rating,
        }
        return await this.renderSubTemplate(identifier, subTemplateVariables);
    }

    renderCourseBannerPurchaseLink = async (bundle, identifier) => {
        let subTemplateVariables = {
            hostName: this.hostOrigin,
            pretty_name: bundle.pretty_name,
            institution_bundle_id : bundle.institution_bundle_id
        }
        return `${subTemplateVariables.hostName}/course/${subTemplateVariables.pretty_name}-${subTemplateVariables.institution_bundle_id}/checkout`;
    }

    renderCourseBannerMedia = async (bundle, identifier) => {
        let subTemplateVariables = {
            course: bundle
        }
        let courseLink = await this.renderSubTemplate('courseBannerVideoLink', subTemplateVariables);
        subTemplateVariables = {
            courseLink: courseLink
        }
        return await this.renderSubTemplate(identifier, subTemplateVariables);
    }

    renderPackageInfoCard = async (bundle, identifier) => {
        let subTemplateVariables = { packageType: bundle.is_online_package ? "Online Program" : "Classroom Program", orgName: bundle.organization_name };
        return await this.renderSubTemplate(identifier, subTemplateVariables);
    }
}

module.exports = SubTemplateRender;