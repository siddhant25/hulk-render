// class DynamicServerSection {
//     constructor(name, section_identifier, html, visible = true, settings = null) {
//         this.name = name;
//         this.section_type = "dynamic_server";
//         this.section_identifier = section_identifier;
//         this.html = html;
//         this.visible = visible;
//         this.settings = settings;
//     }
// }

// const obj = new DynamicServerSection("Section-1", "course_banner", "<div><h1><%= title %></h1><p><%= body %></p><h4><%= author %></h4></div>", true, { course_id: 22 });
// // console.log(obj);

// class DynamicClientSection {
//     constructor(name, section_identifier, html, visible = true, settings = null) {
//         this.name = name;
//         this.section_type = "dynamic_client";
//         this.section_identifier = section_identifier;
//         this.html = html;
//         this.visible = visible;
//         this.settings = settings;
//     }
// }

// const obj2 = new DynamicClientSection("Section-1", "course_curriculum", "<div><h1><%= title %></h1><p><%= body %></p><h4><%= author %></h4></div>", true, { course_id: 22 });
// // console.log(obj2);

// class GenericSection {
//     constructor(name, section_identifier, html, visible = true, settings = null) {
//         this.name = name;
//         this.section_type = "generic";
//         this.section_identifier = section_identifier;
//         this.html = html;
//         this.visible = visible;
//         this.settings = settings;
//     }
// }

// const obj3 = new GenericSection("Section-1", "contact-us", "<div><h1><%= title %></h1><p><%= body %></p><h4><%= author %></h4></div>", true);
// // console.log(obj3);


// class CustomSection {
//     constructor(name, section_identifier, html, visible = true, settings = null) {
//         this.name = name;
//         this.section_type = "custom";
//         this.section_identifier = section_identifier;
//         this.html = html;
//         this.visible = visible;
//         this.settings = settings;
//     }
// }

// const obj4 = new CustomSection("Section-1", "contact-us", "<div><h1><%= title %></h1><p><%= body %></p><h4><%= author %></h4></div>", true);
// // console.log(obj4);

const Constants = {
    DYNAMIC_SERVER: 'dynamic_server',
    DYNAMIC_CLIENT: 'dynamic_client',
    GENERIC: 'generic',
    CUSTOM: 'custom'
}

// const ClassMapper = {
//     'dynamic_server': DynamicServerSection,
//     'dynamic-client': DynamicClientSection,
//     'generic': GenericSection,
//     'custom': CustomSection
// }

class Block {
    constructor(type = 'custom', name, section_identifier = '', html = "", visible = true, settings = {}) {
        this.section_type = type;
        this.name = name;
        this.section_identifier = section_identifier;
        this.html = html;
        this.visible = visible;
        this.settings = settings;
    }
}



console.log(new Block(Constants.GENERIC, "Section-1", "course_curriculum", "<div><h1><%= title %></h1><p><%= body %></p><h4><%= author %></h4></div>", true, { course_id: 22 }));