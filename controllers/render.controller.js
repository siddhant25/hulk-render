const axios = require("axios");
const PageRender = require("../utils/PageRender");

exports.renderPageController = async (req, res, next) => {
    try {
        const pageUrl = req.body.page_url;
        const pageData = {}
        // const pageUrl = "https://awsrekognitionmcahemal.s3.ap-south-1.amazonaws.com/page.html";
        pageData.dynamic_section_keys = req.body.dynamic_section_keys;
        const apiPath = req.body.api_url;
        const response = await axios({ method: "GET", url: pageUrl, timeout: 10000 })
        pageData.page = response.data;
        pageData.page = '<%let element; %> <%let sectionObj; %>' + pageData.page
        // console.log('=======================----------------------========================', pageData);
        const sectionRenderObj = new PageRender(pageData.page, pageData.dynamic_section_keys,apiPath);
        const renderedPlaceHolder = await sectionRenderObj.sectionRenderMap();
        const renderedPage = await sectionRenderObj.renderPage(renderedPlaceHolder);
        console.log(renderedPage);
        res.status(200).json({ page: renderedPage });
    } catch (err) {
        console.log(err);
        const errorObject = { response, message } = err;
        return res.status(err.response.status).json(errorObject);
    }
};
