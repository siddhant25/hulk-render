const renderController = require('../controllers/render.controller');
const middleware = require('../middlewares/middleware');
const express = require('express');
const router = express.Router();

router.post('/render',  renderController.renderPageController);

module.exports = router;