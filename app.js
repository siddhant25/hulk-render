const express = require("express");
const bodyParser = require('body-parser');
if (process.env.NODE_ENV != "production") require("dotenv").config();

const app = express(); // instantiating the express app
app.use(bodyParser.json());  // configuring body parser to parse request bodies as json

app.use((req, res, next) => {
    // res.setHeader("Access-Control-Allow-Origin", "http://localhost:3001"); //Allowing cross-origin Requests to specific domains
    res.setHeader("Access-Control-Allow-Origin", "*"); //Allowing cross-origin Requests to all domains
    res.setHeader("Access-Control-Allow-Methods", "POST"); // Enabling the client to make only get requests.
    res.setHeader("Access-Control-Allow-Headers", "Content-Type"); // Enabling the client to make requests with non default data attached.
    next();
});

const renderRoute = require("./routes/render.routes");

app.use("/", renderRoute);

app.use("/error", (req, res, next) => {
    res.send('error');
});

app.listen(8080, (err) => {
    if (err) {
        console.log(err);
    }
    console.log("Node Server Started on PORT 8080");
});
module.exports = app;