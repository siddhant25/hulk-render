To initialize the project Follow the following steps:

## 1. Install nvm

**For windows**

> https://docs.microsoft.com/en-us/windows/dev-environment/javascript/nodejs-on-windows

**For linux** <br>
<code>sudo apt install curl</code> <br>
<code>curl https://raw.githubusercontent.com/creationix/nvm/master/install.sh | bash</code> <br>
<code>source ~/.profile</code>

## 2. Install node and npm

<code> nvm install 16.14.0  </code> <br>
<code> nvm use 16.14.0</code> <br>

**Set default node version to v6.11.0 to avoid collisions with ember** <br>
<code>nvm alias default 6.11.0</code>

## 3. Installing dependencies
<code> npm install </code>

## 4. Running the server
<code> npm start </code>


